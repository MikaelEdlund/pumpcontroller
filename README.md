# README #

This is software for my Lolin D1 mini basement water pump controller.

### Vad är det här egentligen ###
På våren, när snön smälter, så rinner det alltid in ett par hundra liter vatten in i matkällaren under köket på vårt 1800-talshus.
Med hjälp av en Lolin D1 mini och en Lolin Relay Shield samt en pump och nivåvakt ifrån Xylem så pumpar jag ut vattnet innan det ställer till problem.

### Inkopplingsanvisning ###
Koppla ifrån + på 12V-matningen, via en 4A säkring, till ena polen på NO-plinten på reläet.
Koppla ifrån andra polen på NO-plinten till + på pumpen.
Koppla ifrån - på 12V-matningen till - på pumpen.
Koppla D2 på CPUn till ena sidan av nivåvakten. Koppla GND till andra sidan.

### Princip ###
Nivåvakten sluter kretsen när vattennivån blir för hög. D2 dras då låg.
När det sker så sluter relät 12V-matningen till pumpen.

