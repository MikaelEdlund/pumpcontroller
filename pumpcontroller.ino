
enum State {
  WAIT,
  WATERLEVELCHECK,
  DEBOUNCE1,
  DEBOUNCE2,
  PUMPSTART,
  PUMPSTOP,
  PUMPCHECK1,
  PUMPCHECK2,
  PUMPCHECK3,
  REPORTPUMPEVENT
};

const char* StateS[] = {
  "WAIT",
  "WATERLEVELCHECK",
  "DEBOUNCE1",
  "DEBOUNCE2",
  "PUMPSTART",
  "PUMPSTOP",
  "PUMPCHECK1",
  "PUMPCHECK2",
  "PUMPCHECK3",
  "REPORTPUMPEVENT"
};

#define BUFFERSIZE 40
char logBuffer[BUFFERSIZE];

int timeShort  = 100;
int timeLong   = 500;
int timeSettle = 5000;
int timePump   = 20000;
int timeToNext = 60000;

int previousState = -1;
int presentState  = -1;
int nextState     = -1;

unsigned long waitStart    = -1;
unsigned long waitDuration = -1;

int pumpCheck1 = -1;
int pumpCheck2 = -1;
int pumpCheck3 = -1;

const int WATER_LOW  = 0;
const int WATER_HIGH = 1;

const int RELAY  = D1;
const int SWITCH = D2;

void setup() {
  Serial.begin(9600);
  pinMode(RELAY, OUTPUT);
  pinMode(SWITCH, INPUT_PULLUP);

  digitalWrite(RELAY, LOW);
  SetupSleep(timeLong, WATERLEVELCHECK);
}

void loop()
{
  switch(presentState)
  {
    case WAIT:
      LogStateChange(WAIT);
      HandleWait();
    break;

    case WATERLEVELCHECK:
      LogStateChange(WATERLEVELCHECK);
      HandleWaterLevelCheck();
    break;

    case DEBOUNCE1:
      LogStateChange(DEBOUNCE1);
      HandleDebounce1();
    break;

    case DEBOUNCE2:
      LogStateChange(DEBOUNCE2);
      HandleDebounce2();
    break;

    case PUMPSTART:
      LogStateChange(PUMPSTART);
      HandlePumpStart();
    break;

    case PUMPSTOP:
      LogStateChange(PUMPSTOP);
      HandlePumpStop();
    break;

    case PUMPCHECK1:
      LogStateChange(PUMPCHECK1);
      HandlePumpCheck1();
    break;

    case PUMPCHECK2:
      LogStateChange(PUMPCHECK2);
      HandlePumpCheck2();
    break;

    case PUMPCHECK3:
      LogStateChange(PUMPCHECK3);
      HandlePumpCheck3();
    break;

    case REPORTPUMPEVENT:
      LogStateChange(REPORTPUMPEVENT);
      HandleReportPumpEvent();
    break;
  }
}



// State handlers
void HandleWait() {
  if (millis() - waitStart >= waitDuration)
    presentState = nextState;
}

void HandleWaterLevelCheck() {
  if (ReadSensor() == WATER_HIGH)
    SetupSleep(timeShort, DEBOUNCE1);
  else
    SetupSleep(timeLong, WATERLEVELCHECK);
}

void HandleDebounce1() {
  if (ReadSensor() == WATER_HIGH)
    SetupSleep(timeShort, DEBOUNCE2);
  else
    SetupSleep(timeLong, WATERLEVELCHECK);
}

void HandleDebounce2() {
  if (ReadSensor() == WATER_HIGH)
    SetupSleep(timeShort, PUMPSTART);
  else
    SetupSleep(timeLong, WATERLEVELCHECK);
}

void HandlePumpStart() {
  SetupSleep(timePump, PUMPSTOP);
  digitalWrite(RELAY, HIGH);
}

void HandlePumpStop() {
  SetupSleep(timeSettle, PUMPCHECK1);
  digitalWrite(RELAY, LOW);
}

void HandlePumpCheck1() {
  SetupSleep(timeSettle, PUMPCHECK2);
  pumpCheck1 = ReadSensor();
}

void HandlePumpCheck2() {
  SetupSleep(timeSettle, PUMPCHECK3);
  pumpCheck2 = ReadSensor();
}

void HandlePumpCheck3() {
  SetupSleep(timeShort, REPORTPUMPEVENT);
  pumpCheck3 = ReadSensor();
}

void HandleReportPumpEvent() {
  SetupSleep(timeToNext, WATERLEVELCHECK);

  snprintf(logBuffer, BUFFERSIZE, "%d|%d|%d", pumpCheck1, pumpCheck2, pumpCheck3);
  Serial.println(logBuffer);
}



// Helpers
void LogStateChange(uint8_t state) {
  if (previousState != state) {
    previousState = state;

    snprintf(logBuffer, BUFFERSIZE, "%ld: %s", millis()/1000, StateS[state]);
    Serial.println(logBuffer);
  }
}

void SetupSleep(long duration, uint8_t state) {
  waitStart    = millis();
  waitDuration = duration;
  presentState = WAIT;
  nextState    = state;
}

int ReadSensor() {
  return !digitalRead(SWITCH);
}
